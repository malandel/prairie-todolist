### Objectifs
Réaliser une todo list permettant d'ajouter des choses à se rappeler. Une fois qu'une chose est faite, la rayer. Rajouter la possibilité de n'afficher que les tâches faites, les tâches à faire ou toutes les tâches.

### Adresse du site 

http://malandel.gitlab.io/prairie-todolist 

### Adresse Kanban Board (Trello)

https://trello.com/b/up4JOzRO/prairie-todolist


### Lien de l'exercice

https://gitlab.com/labege_carbonne/TodoList
