// Créer une nouvelle tâche en cliquant sur "Valider" (onclick dans le HTML)
function newElement() {
    var li = document.createElement("li");
    var inputValue = document.getElementById("userTask").value;
    var t = document.createTextNode(inputValue);
    li.appendChild(t);
    if (inputValue === '') {
      alert("Vous devez écrire une tâche !");
    } else {
      document.getElementById("myList").appendChild(li);
    }
    document.getElementById("userTask").value = "";
};

// Ajouter une classe "checked" quand une tâche est cliquée
var list = document.querySelector('ul');
list.addEventListener('click', function(ev) {
  if (ev.target.tagName === 'LI') {
    ev.target.classList.toggle('checked');
  }
}, false);